package de.cantry.scamsiteevaluator;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Evaluator {


    public int evaluate(String url) throws Exception {
        int score = 0;
        Http http = new Http();
        String pagesource = http.get(url);
        List<String> imgurs = regexFindAll("(imgur.com/[^(\"|\\))]+)(\"|\\))",pagesource);
        for(String s : imgurs){
            score++;
        }
        List<String> javascripts = regexFindAll("script src=\"([^\"]+)\"",pagesource);
        List<String> commonjs = Files.readAllLines(Paths.get("commonjs.txt"));
        for(String cjs : commonjs){
            for(String js : javascripts){
                if(js.contains(cjs)){
                    score++;
                }
            }
        }
        return score;
    }

    public static List<String> regexFindAll(String pattern, String text){
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(text);
        List<String> matches = new ArrayList<>();
        while (m.find()) {
            matches.add(m.group(1));
        }
        return matches;
    }

}
