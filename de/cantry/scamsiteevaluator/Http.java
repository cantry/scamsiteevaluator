package de.cantry.scamsiteevaluator;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Http {

    public static HttpURLConnection con;

    public String get(String url)throws Exception{

        URL target = new URL(url);

        HttpURLConnection connection = (HttpURLConnection) target.openConnection();
        con = connection;
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0");
        connection.setRequestProperty("Accept-Charset", "UTF-8");
        connection.setRequestProperty("Accept-Language", "de,en-US;q=0.7,en;q=0.3");
        connection.setRequestProperty("Accept", "*/*");
        connection.setRequestMethod("GET");

        InputStream response = connection.getInputStream();
        BufferedReader in = new BufferedReader(new InputStreamReader(response, "UTF-8"));
        StringBuilder sb = new StringBuilder();

        if(connection.getHeaderFields().get("Location") != null){
            System.out.println("following redirect " + connection.getHeaderFields().get("Location").get(0));
            return get(connection.getHeaderFields().get("Location").get(0));
        }
        String result;
        while((result = in.readLine()) != null)
        {
            sb.append(result);
        }

        in.close();

        String str = sb.toString();

        return str;
    }

}
