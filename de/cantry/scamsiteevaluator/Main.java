package de.cantry.scamsiteevaluator;

public class Main {

    public static void main(String[] args) throws Exception {
        Evaluator ev = new Evaluator();
        String checked = checkURL(args[0]);
        System.out.println(checked);
        int score = ev.evaluate(checked);
        if(score != 0){
            System.out.println(args[0] + " is probably a scam site " + score + " common scripts/images found");
        }
    }

    public static String checkURL(String url) {
        if(!url.contains("www") && url.contains("http")){
            url = url.split("//")[1];
        }
        if(!(url.startsWith("http://www.") || url.startsWith("https://www."))){
            if(url.startsWith("www.")){
                url = "http://" + url;
            }else{
                if(!url.startsWith("http")){
                    url = "http://www." + url;
                }
            }
        }
        if(!url.startsWith("http")){
            url = "http://www." + url;
        }
        if(!url.endsWith("/")){
            url += "/";
        }
        return url.replace(" ","");
    }
}
